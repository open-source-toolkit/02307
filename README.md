# SpringBoot + Vue3 权限管理系统 Demo 源码

本项目是一个基于 SpringBoot 和 Vue3 的前后台分离权限管理系统 Demo 演示程序。后台采用 SpringBoot 架构，前台使用 Vue3 和 Element Plus 组件库，数据库使用 MySQL。目前，前台和后台系统已经完全调通，下载源码后，加载到 IDE 即可运行。

## 项目结构

压缩包内包含以下文件夹和文件：

1. **yaken_AuthorizationManagementDemo** - SpringBoot 后台程序
   - 导入后请将其转换为 Maven 工程。
   - 根据 `resources/application.yml` 文件内容配置数据库和头像文件夹路径信息。

2. **yaken_AuthorizationManagementDemo_vue** - 前台程序
   - 导入 IDE 后，首先运行 `npm install` 下载必要的依赖。
   - 执行 `npm run serve` 启动程序。
   - 启动程序前，请修改 `权限管理Demo/yaken_AuthorizationManagementDemo_vue/src/util/request.js` 文件内的 `baseUrl` 项的 IP 和端口。如果不知道自己的 IP，可以将其修改为 `127.0.0.1`。

3. **yakenamd.sql** - 数据表结构文件
   - 包含项目所需的数据库表结构。

4. **userAvatar** - 头像文件夹
   - 用于存储用户头像文件。

## 运行步骤

1. **导入后台程序**
   - 将 `yaken_AuthorizationManagementDemo` 文件夹导入到 IDE 中，并将其转换为 Maven 工程。
   - 根据 `resources/application.yml` 文件配置数据库连接信息和头像文件夹路径。

2. **导入前台程序**
   - 将 `yaken_AuthorizationManagementDemo_vue` 文件夹导入到 IDE 中。
   - 运行 `npm install` 下载依赖。
   - 修改 `src/util/request.js` 文件中的 `baseUrl` 项，配置正确的 IP 和端口。
   - 运行 `npm run serve` 启动前台程序。

3. **导入数据库**
   - 使用 `yakenamd.sql` 文件创建数据库表结构。

4. **启动项目**
   - 先启动 SpringBoot 后台程序，再启动 Vue3 前台程序。

## 注意事项

- 确保数据库连接信息正确无误。
- 前台程序启动前，务必修改 `request.js` 文件中的 `baseUrl` 配置。
- 如果遇到任何问题，请检查配置文件和依赖是否正确安装。

## 联系我们

如有任何问题或建议，欢迎联系我们。